package hu.pont.gendersoundanalyzer.utils;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.skd.androidrecording.visualizer.AudioData;
import com.skd.androidrecording.visualizer.FFTData;
import com.skd.androidrecording.visualizer.renderer.Renderer;

import hu.pont.gendersoundanalyzer.interfaces.FftDataAnalyzeListener;

/**
 * Created by Rcsk on 02/11/2014.
 */
public class AnalyzeBarRenderer extends Renderer {

    private int mDivisions;
    private Paint mPaint;
    private boolean mTop;

    private FftDataAnalyzeListener mFftDataAnalyzeListener;

    public FftDataAnalyzeListener getmFftDataAnalyzeListener() {
        return mFftDataAnalyzeListener;
    }

    public void setmFftDataAnalyzeListener(FftDataAnalyzeListener mFftDataAnalyzeListener) {
        this.mFftDataAnalyzeListener = mFftDataAnalyzeListener;
    }

    /**
     * Renders the FFT data as a series of lines, in histogram form
     *
     * @param divisions - must be a power of 2. Controls how many lines to draw
     * @param paint     - Paint to draw lines with
     * @param top       - whether to draw the lines at the top of the canvas, or the bottom
     */


    public AnalyzeBarRenderer(int divisions,
                              Paint paint,
                              boolean top) {
        super();
        mDivisions = divisions;
        mPaint = paint;
        mTop = top;
    }

    @Override
    public void onRender(Canvas canvas, AudioData data, Rect rect) {
        // Do nothing, we only display FFT data
    }

    int mintavetelsec = 0;


    @Override
    public void onRender(Canvas canvas, FFTData data, Rect rect) {
        for (int i = 0; i < data.getBytes().length / mDivisions; i++) {
            mFFTPoints[i * 4] = i * 4 * mDivisions;
            mFFTPoints[i * 4 + 2] = i * 4 * mDivisions;
            byte rfk = data.getBytes()[mDivisions * i];
            byte ifk = data.getBytes()[mDivisions * i + 1];
            float magnitude = (rfk * rfk + ifk * ifk);
            int dbValue = (int) (10 * Math.log10(magnitude));


            if(mFftDataAnalyzeListener != null){
                if(mintavetelsec %10 == 0) {
                    mFftDataAnalyzeListener.updateValue(dbValue, i);
                }
            }

            if (mTop) {
                mFFTPoints[i * 4 + 1] = 0;
                mFFTPoints[i * 4 + 3] = (dbValue * 2 - 10);
            } else {
                mFFTPoints[i * 4 + 1] = rect.height();
                mFFTPoints[i * 4 + 3] = rect.height() - (dbValue * 2 - 10);
            }
        }

        mintavetelsec++;

        canvas.drawLines(mFFTPoints, mPaint);
    }
}

