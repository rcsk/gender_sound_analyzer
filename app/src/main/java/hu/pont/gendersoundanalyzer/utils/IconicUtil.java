package hu.pont.gendersoundanalyzer.utils;

import android.content.Context;
import android.graphics.Color;

import com.atermenji.android.iconicdroid.IconicFontDrawable;
import com.atermenji.android.iconicdroid.icon.Icon;

/**
 * Created by Rcsk on 06/10/2014.
 */
public class IconicUtil {


    public static IconicFontDrawable getIconicFontDrawable(Icon icon, Context context){

        IconicFontDrawable fontDrawable = new IconicFontDrawable(context);
        fontDrawable.setIcon(icon);
        fontDrawable.setIconColor(Color.WHITE);
        fontDrawable.setIntrinsicWidth(60);
        fontDrawable.setIntrinsicHeight(60);
        return  fontDrawable;
    }


}
