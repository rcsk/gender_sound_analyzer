package hu.pont.gendersoundanalyzer.beans;

import android.content.Context;
import android.util.Log;

import com.skd.androidrecording.audio.AudioRecordingThread;
import com.skd.androidrecording.visualizer.FFTData;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import hu.pont.gendersoundanalyzer.R;
import hu.pont.gendersoundanalyzer.activities.AudioRecordingActivity;
import hu.pont.gendersoundanalyzer.interfaces.FftDataAnalyzeListener;
import hu.pont.gendersoundanalyzer.utils.NotificationUtils;

/**
 * Created by Rcsk on 02/11/2014.
 */
@EBean
public class GenderAnalyzer implements FftDataAnalyzeListener{

    @RootContext
    Context context;

    private static final String TAG = "GenderAnalyzer";

    int[] amplitudoAvarage = null;
    int[] indexes;

    @Override
    public void onStart() {
        amplitudoAvarage = null;
        amplitudoAvarage = new int[AudioRecordingThread.FFT_POINTS];
        indexes = new int[AudioRecordingThread.FFT_POINTS];
        for(int i= 0; i< amplitudoAvarage.length; i++){
            indexes[i] = i+1;
            amplitudoAvarage[i] = 0;
        }
    }

    @Override
    public void updateValue(int dbValue, int position) {
        if(amplitudoAvarage != null) {
            int temp = (dbValue + amplitudoAvarage[position]) / 2;
            amplitudoAvarage[position] = temp;
        }
    }

    @Override
    public void onFinish() {

        try {

            Log.i(TAG, "onFinish " + amplitudoAvarage);
            quicksort(amplitudoAvarage, indexes, 0, indexes.length - 1);


            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < indexes.length; i++) {
                sb.append(" ");
                sb.append(indexes[i]);
            }

            int womanCount = 0;
            for (int i = 0; i < indexes.length; i++) {
                if (indexes[i] > 512) {
                    womanCount++;
                }
            }

            Log.i(TAG, "onFinish " + sb.toString());
            Log.i(TAG, "onFinish genderalayuer");

            if(womanCount > 500){
                NotificationUtils.showInfoDialog(context, "Inkábbb nő");
            }else{
                NotificationUtils.showInfoDialog(context, "Inkább férfi");
            }



        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void quicksort(int[] main, int[] index) {
        quicksort(main, index, 0, index.length - 1);
    }

    // quicksort a[left] to a[right]
    public static void quicksort(int[] a, int[] index, int left, int right) {
        if (right <= left) return;
        int i = partition(a, index, left, right);
        quicksort(a, index, left, i-1);
        quicksort(a, index, i+1, right);
    }

    // partition a[left] to a[right], assumes left < right
    private static int partition(int[] a, int[] index,
                                 int left, int right) {
        int i = left - 1;
        int j = right;
        while (true) {
            while (less(a[++i], a[right]))      // find item on left to swap
                ;                               // a[right] acts as sentinel
            while (less(a[right], a[--j]))      // find item on right to swap
                if (j == left) break;           // don't go out-of-bounds
            if (i >= j) break;                  // check if pointers cross
            exch(a, index, i, j);               // swap two elements into place
        }
        exch(a, index, i, right);               // swap with partition element
        return i;
    }

    // is x < y ?
    private static boolean less(float x, float y) {
        return (x < y);
    }

    // exchange a[i] and a[j]
    private static void exch(int[] a, int[] index, int i, int j) {
        int swap = a[i];
        a[i] = a[j];
        a[j] = swap;
        int b = index[i];
        index[i] = index[j];
        index[j] = b;
    }

}
