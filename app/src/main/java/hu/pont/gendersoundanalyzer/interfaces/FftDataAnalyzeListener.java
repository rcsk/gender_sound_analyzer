package hu.pont.gendersoundanalyzer.interfaces;

import com.skd.androidrecording.visualizer.FFTData;

/**
 * Created by Rcsk on 02/11/2014.
 */
public interface FftDataAnalyzeListener {
    void onStart();
    void updateValue(int dbValue, int position);
    void onFinish();
}
