package hu.pont.gendersoundanalyzer.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import org.androidannotations.annotations.EActivity;

import hu.pont.gendersoundanalyzer.R;

@EActivity
public class MainActivity extends BaseActivity {

    Button recordButton;
    Button theoryButton;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        recordButton = (Button) findViewById(R.id.mainButton);
        theoryButton = (Button)findViewById(R.id.theoryButton);

        theoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TheoryActivity_.intent(MainActivity.this).start();
            }
        });

        theoryButton.setVisibility(View.GONE);

        recordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AudioRecordingActivity_.intent(MainActivity.this).start();
            }
        });

        toolbar.setTitle(getTitle());
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_my;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
