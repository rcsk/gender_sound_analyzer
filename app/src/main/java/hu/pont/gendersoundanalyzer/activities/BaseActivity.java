package hu.pont.gendersoundanalyzer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import hu.pont.gendersoundanalyzer.R;
import hu.pont.gendersoundanalyzer.utils.ErrorUtil;

/**
 * Created by Rcsk on 06/10/2014.
 */
public abstract  class BaseActivity  extends ActionBarActivity{

    protected Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        }

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler(){
            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                String cause = ErrorUtil.getCause(e);
                String errorType = ErrorUtil.getExceptionType(e);
                String stackTrace = ErrorUtil.getStrackTrace(e);
                String deviceInfo = ErrorUtil.getDeviceInfo();

                Intent i = new Intent(BaseActivity.this, ErrorActivity_.class);
                i.putExtra(ErrorActivity.EXCEPTION_TYPE_ARG, errorType);
                i.putExtra(ErrorActivity.STACKTRACE_ARG, stackTrace);
                i.putExtra(ErrorActivity.DEVICE_INFO_ARG, deviceInfo);
                i.putExtra(ErrorActivity.CAUSE_ARG, cause);

                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                System.exit(0);
            }
        });

    }
    protected abstract int getLayoutResource();


    protected void setActionBarIcon(int iconRes) {
        toolbar.setNavigationIcon(iconRes);
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
        }

        return super.onOptionsItemSelected(item);
    }
}
