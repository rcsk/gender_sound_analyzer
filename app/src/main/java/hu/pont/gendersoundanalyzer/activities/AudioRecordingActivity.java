package hu.pont.gendersoundanalyzer.activities;

/**
 * Created by Rcsk on 02/10/2014.
 */

import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;

import com.atermenji.android.iconicdroid.icon.EntypoIcon;
import com.skd.androidrecording.audio.AudioPlaybackManager;
import com.skd.androidrecording.audio.AudioRecordingHandler;
import com.skd.androidrecording.audio.AudioRecordingThread;
import com.skd.androidrecording.video.MediaPlayerManager;
import com.skd.androidrecording.video.PlaybackHandler;
import com.skd.androidrecording.visualizer.VisualizerView;
import com.skd.androidrecording.visualizer.renderer.BarGraphRenderer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;

import at.markushi.ui.CircleButton;
import hu.pont.gendersoundanalyzer.R;
import hu.pont.gendersoundanalyzer.beans.GenderAnalyzer;
import hu.pont.gendersoundanalyzer.utils.AnalyzeBarRenderer;
import hu.pont.gendersoundanalyzer.utils.IconicUtil;
import hu.pont.gendersoundanalyzer.utils.NotificationUtils;
import hu.pont.gendersoundanalyzer.utils.StorageUtils;

@EActivity
public class AudioRecordingActivity extends BaseActivity implements PlaybackHandler {
    private static String fileName = null;

    private MediaPlayerManager playerManager;

    private static final String TAG = "AudioRecordingActivity";
    private CircleButton recordBtn;
    private VisualizerView visualizerView;

    private AudioRecordingThread recordingThread;
    private boolean startRecording = true;

    private AudioPlaybackManager playbackManager;
    private AnalyzeBarRenderer barGraphRendererBottom;

    @Bean
    GenderAnalyzer genderAnalyzer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!StorageUtils.checkExternalStorageAvailable()) {
            NotificationUtils.showInfoDialog(this, getString(R.string.noExtStorageAvailable));
            return;
        }
        afterViews();

    }

    @AfterViews
    void afterViews() {
        fileName = StorageUtils.getFileName(true);

        recordBtn = (CircleButton) findViewById(R.id.recordBtn);
        recordBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                record();
            }
        });

        recordBtn.setImageDrawable(IconicUtil.getIconicFontDrawable(EntypoIcon.PLAY, this));

        visualizerView = (VisualizerView) findViewById(R.id.visualizerView);
        setupVisualizer();

        playbackManager = new AudioPlaybackManager(this, visualizerView, this);
        playbackManager.hideMediaController();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.audio_rec;
    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseVisualizer();
        recordStop();
        playbackManager.pause();
        playbackManager.hideMediaController();
    }

    @Override
    protected void onDestroy() {
        recordStop();
        releaseVisualizer();

        super.onDestroy();
    }

    private void setupVisualizer() {
        Paint paint = new Paint();
        paint.setStrokeWidth(5f);
        paint.setAntiAlias(true);
        paint.setColor(Color.argb(200, 227, 69, 53));
         barGraphRendererBottom = new AnalyzeBarRenderer(2, paint, false);
        barGraphRendererBottom.setmFftDataAnalyzeListener(genderAnalyzer);
        visualizerView.addRenderer(barGraphRendererBottom);
        visualizerView.invalidate();
    }

    private void releaseVisualizer() {
        try {
            visualizerView.release();
            visualizerView = null;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void record() {
        if (startRecording) {
            recordStart();
        } else {
            recordStop();
        }
    }

    private void recordStart() {
        startRecording();
        startRecording = false;
        recordBtn.setImageDrawable(IconicUtil.getIconicFontDrawable(EntypoIcon.STOP, this));

        playbackManager.hideMediaController();

    }

    private void recordStop() {
        try {
            stopRecording();
            startRecording = true;
            recordBtn.setImageDrawable(IconicUtil.getIconicFontDrawable(EntypoIcon.PLAY, this));

            visualizerView.clearRenderers();
            setupVisualizer();

            playbackManager.showMediaController();

            genderAnalyzer.onFinish();

            playbackManager.setupPlayback(fileName);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Background
    void startRecording() {
        genderAnalyzer.onStart();
        recordingThread = new AudioRecordingThread(fileName, new AudioRecordingHandler() {
            @Override
            public void onFftDataCapture(final byte[] bytes) {
                updateVisualizer(bytes);
            }

            @Override
            public void onRecordSuccess() {
                playbackManager.setupPlayback(fileName);
            }

            @Override
            public void onRecordingError() {
                showRecordErrorDialog();
            }

            @Override
            public void onRecordSaveError() {
                stopRec();
            }
        });
        recordingThread.start();
    }

    @UiThread
    void updateVisualizer(byte[] bytes) {
        visualizerView.updateVisualizerFFT(bytes);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(startRecording) {
            playbackManager.showMediaController();
        }
        return false;
    }

    @UiThread
    void showRecordErrorDialog() {
        recordStop();
        NotificationUtils.showInfoDialog(this, getString(R.string.recordingError));
    }

    @UiThread
    void stopRec() {
        recordStop();
        NotificationUtils.showInfoDialog(this, getString(R.string.saveRecordError));
    }


    @Background
    void stopRecording() {
        if (recordingThread != null) {
            recordingThread.stopRecording();
            recordingThread = null;
        }
    }


    @Override
    public void onPreparePlayback() {
        try {
            visualizerView.link(playbackManager.getPlayerManager().getPlayer());
            playbackManager.showMediaController();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}