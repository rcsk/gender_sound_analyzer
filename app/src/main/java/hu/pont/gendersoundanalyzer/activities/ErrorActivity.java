package hu.pont.gendersoundanalyzer.activities;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.ViewsById;

import hu.pont.gendersoundanalyzer.R;

@EActivity
public class ErrorActivity extends BaseActivity {


    public final static String STACKTRACE_ARG = "strack-arg";
    public final static String EXCEPTION_TYPE_ARG ="extype-arg";
    public final static String CAUSE_ARG ="cause-arg";
    public final static String DEVICE_INFO_ARG ="devinf-arg";

    @ViewById(R.id.errorStactTraceTextView)
    TextView errorStactTraceTextView;

    @ViewById(R.id.errorConforimButton)
    Button errorConfirmButton;

    @ViewById(R.id.errorDeviceInfoTextView)
    TextView errorDeviceInfoTextView;

    @ViewById(R.id.errorCauseTextView)
    TextView errorCauseTextView;

    @ViewById(R.id.errorExceptionTypeTextView)
    TextView errorExceptionTypeTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getIntent().hasExtra(CAUSE_ARG)){
            errorCauseTextView.setText(getIntent().getStringExtra(CAUSE_ARG));
        }

        if(getIntent().hasExtra(DEVICE_INFO_ARG)){
            errorDeviceInfoTextView.setText(getIntent().getStringExtra(DEVICE_INFO_ARG));
        }


        if(getIntent().hasExtra(EXCEPTION_TYPE_ARG)){
            errorExceptionTypeTextView.setText(getIntent().getStringExtra(EXCEPTION_TYPE_ARG));
        }

        if(getIntent().hasExtra(STACKTRACE_ARG)){
            errorStactTraceTextView.setText(getIntent().getStringExtra(STACKTRACE_ARG));
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_error;
    }


    @Click(R.id.errorConforimButton)
    public void confirmException(){
        //Intent intent = new Intent(this, SplashActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        //startActivity(intent);
        finish();
        //overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.error, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
